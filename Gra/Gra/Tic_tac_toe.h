#pragma once
#include <cstdlib>
#include <iostream>
#include <ctime>
#include <Windows.h>
#include <mmsystem.h>
#include <algorithm>
#include <limits>
#include <iomanip>

using namespace std;

bool wygrana(char field[], int size, char symbol, int ile) {
	int zlicz(0);

	for (int i(0); i < size*size; i++) {
		for (int j(0); j < size*size; j += size + 1) {
			if (field[i + j] == symbol)
				zlicz++;
			else
				break;
			if ((((i + j) - (size - 1)) % size) == 0) break;
		}
		if (zlicz == ile) return true; zlicz = 0;
	}

	for (int i(0); i < size*size; i++) {
		for (int j(0); j < size*size; j += size - 1) {
			if (field[i + j] == symbol)
				zlicz++;
			else
				break;
			if ((i + j) % size == 0) break;
		}
		if (zlicz == ile) return true; zlicz = 0;
	}

	for (int i(0); i < size*size; i++) {//w linii pionowej
		for (int j(0); j <= ((ile - 1)*size); j += size) {
			if (field[i + j] == symbol)
				zlicz++;
			else
				break;
		}
		if (zlicz == ile) return true; zlicz = 0;
	}

	for (int i(0); i < size*size; i++) {//w linii poziomej
		for (int j(0); j < ile; j++) {
			if (field[i + j] == symbol)
				zlicz++;
			else
				break;
			if ((((i + j) - (size - 1)) % size) == 0) break;
		}
		if (zlicz == ile) return true; zlicz = 0;
	}


	return false;
}

void wyswietl(char field[], int size) {//wyswietla zawartosc pola
	std::cout << endl;
	for (int i(0); i < size*size; i++) {
		if (i % (size) == 0 && i!=0) {
			std::cout << endl;
			for (int j(0); j < size-1; j++)
				std::cout << "----+";
			std::cout << "----\n";
		}
		if (field[i] != ' ')
			std::cout << ' '<<setw(2)<<field[i]<<' ';
		else
			std::cout << ' '<<setw(2)<<i<<' ';
		if ((i+1) % size != 0 )std::cout << "|";
	}
	std::cout << endl<<endl;
}

bool czy_zajete(char field[], int size) { // Sprawdza czy cale pole zostalo juz zajete, jesli tak zwraca true(rownoznaczne z remisem)

	for (int i(0); i < size*size; i++)
		if (field[i] == ' ')
			return false;
	return true;
}

int first(char field[], int size) {//zwraca wylosowana pozycje dla pierwszego ruchu kopmutera
	int pos(0);
	srand(time(NULL));
	do {
		pos = rand() % (size*size);
	} while (field[pos]!=' ');
	return pos;
}

int alfa_beta(char field[], int size, char symbol, char PC, int ile, int glebokosc, int alfa, int beta, int &adres,int ruch) {
	if (wygrana(field, size, symbol, ile)) return(symbol == PC) ? (size*size - ruch) : (-1)*(size*size - ruch); 
	else if (czy_zajete(field, size))  return 0;
	if (glebokosc == 0) return(symbol == PC) ? (-ruch) : ruch;


	symbol = (symbol == 'X') ? 'O' : 'X';

	if (symbol == PC) {
		int V = -1000;
		for (int i(0); i < size*size; i++) {
			if (field[i] == ' ') {
				field[i] = symbol;
				V = max(V, alfa_beta(field, size, symbol, PC, ile, glebokosc - 1, alfa, beta, adres,ruch+1));
				alfa = max(alfa, V);
				field[i] = ' ';
			}
			if (alfa >= beta) break; 
		}
		return V;
	}
	else {
		int V = 1000;
		for (int i(0); i < size*size; i++) {
			if (field[i] == ' ') {
				field[i] = symbol;
				V = min(V, alfa_beta(field, size, symbol, PC, ile, glebokosc - 1, alfa, beta, adres,ruch+1));
				beta = min(beta, V);
				field[i] = ' ';
			}
			if (alfa >= beta) break;
		}
		return V;
	}
}

int PC_move(char field[], int size, char PC, int ile) {
	int naj = -1000,pos(0),wsp(0);

	for (int i(0); i < size*size; i++) {
		if (field[i] == ' ') {
			field[i] = PC;
			wsp = alfa_beta(field, size, PC, PC, ile, ile+2, -1000, 1000, pos,1);
			field[i] = ' ';
			if (wsp > naj) { naj = wsp; pos = i; }
		}
	}
	return pos;
}


void ruch(char field[], int size,int war, char &player, char PC, int ktory) {
	//static int ile(0);
	int pos(0);

	if (player == PC) {
		if (ktory == 0) {
			pos = first(field, size);
			//ktory++;
		}
		else {
			pos = PC_move(field, size, PC, war);
		}
	}
	else {
		std::cout << "Twoj ruch, podaj pozycje:\n";
		std::cin >> pos;
		while (pos<0 || pos>size*size) {
			std::cout << "poza zakresem, jeszcze raz\n";
			std::cin >> pos;
		}
		while (field[pos] != ' ') {
			std::cout << "pole juz zajete, sprobuj jeszcze raz\n";
			std::cin >> pos;
		}
		
		//if (ktory == 0) ktory++;
	}
	if (field[pos] != ' ')cout << "\nstrange\n";
	if (pos >= 0 && pos <= size*size && field[pos] == ' ') field[pos] = player;
	else cerr << "cos sie popsu�o\n";
	player = (player == 'X') ? 'O' : 'X';
}

void tic_tac_toe() {
	int size(0), ile(0), wyb(0);
	char player='O', PC='X';

	HANDLE hOut= GetStdHandle(STD_OUTPUT_HANDLE);;
	SetConsoleTextAttribute(hOut, FOREGROUND_RED| FOREGROUND_INTENSITY| BACKGROUND_BLUE);
	std::cout << "-------KOLKO I KRZYZYK------\n\n";
	SetConsoleTextAttribute(hOut, FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_RED);
	std::cout << "Podaj prosze rozmiar planszy:   ";
	std::cin >> size;
	std::cout << "Warunkiem ma byc ile znakow w linii?\n";

	do {
		if (ile > size)
			std::cout << "za duzo\n";
		std::cin >> ile;
	} while (ile > size);
	std::cout << endl;

	char *field = new char[size*size];
	for (int j(0); j < size*size; j++)
		field[j] = ' ';

	wyb = 0;
	std::cout << "Ktorym znakiem chcesz grac? 1 - X, 2 - O ?\n";
	do {
	std::cin >> wyb;
		if (wyb == 1)
			player = 'X', PC = 'O';
		if (wyb != 1 && wyb != 2)cout << "zly wybor\n";
	} while (wyb != 1 && wyb != 2);

	wyb = 0;
	std::cout << "kto ma zaczynac? 1 - Ty, 2 - komputer ?";
	do {
		std::cin >> wyb;
		if (wyb == 2)
			player = PC;
		if (wyb != 1 && wyb != 2)cout << "zly wybor\n";
	} while (wyb != 1 && wyb != 2);

	SetConsoleTextAttribute(hOut, FOREGROUND_RED | FOREGROUND_INTENSITY | BACKGROUND_BLUE);
	int l(0);
	while (!wygrana(field, size,'O', ile) && !wygrana(field, size, 'X', ile) && !czy_zajete(field, size)) {//wlasciwa rozgrywka
		wyswietl(field, size);
		ruch(field, size,ile, player, PC,l);
		if (l == 0) l++;
		system("cls");
	}

	wyswietl(field, size);
	if (PC == 'O')player = 'X';
	else player = 'O';
	if (wygrana(field, size, PC, ile))	std::cout << "PRZZEGRALES!!!\n";
	else if(wygrana(field, size, player, ile)) std::cout << "WYGRALES!!!\n";
	else if (czy_zajete(field, size))	std::cout << "REMIS\n";
	//SetConsoleTextAttribute(hOut, FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE | BACKGROUND_RED);
	//SetConsoleTextAttribute(hOut, FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE | BACKGROUND_RED/*| BACKGROUND_INTENSITY*/);
	std::cout << "xexexe\n";
	delete[] field;
}