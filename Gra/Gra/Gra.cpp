// Gra.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include "Tic_tac_toe.h"
#include <process.h>

using namespace std;

void sound(void *ignored) {
	PlaySound(TEXT("Drug_song.wav"), NULL, SND_LOOP | SND_FILENAME);
	_endthread();
}

int main()
{
	_beginthread(sound, 0, NULL);
	int wybor(0);

	do {
		tic_tac_toe();
		cout << "Czy chcialbys zagrac jeszcze raz? (Nacisnij '0' i Enter aby wyjsc z gry)\n";
		cin >> wybor;
	} while (wybor != 0);

	//system("PAUSE");
    return 0;
}

